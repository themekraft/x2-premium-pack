<?php

/**
 * Admin Menu Class
 *
 * @package Update API Manager/Admin
 * @author Todd Lahman LLC
 * @copyright   Copyright (c) Todd Lahman LLC
 * @since 1.3
 *
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class X2_API_Manager_MENU {

	private $x2_api_manager_key;

	// Load admin menu
	public function __construct() {

		$this->x2_api_manager_key = new X2_API_Manager_Key();

		add_action( 'admin_menu', array( $this, 'add_menu' ) );
		add_action( 'admin_init', array( $this, 'load_settings' ) );
	}

	// Add option page menu
	public function add_menu() {

        $page = add_options_page( __( X2AM()->x2_plugin_settings_menu_title, 'x2pp' ), __( X2AM()->x2_plugin_settings_menu_title, 'x2pp' ),
						'edit_theme_options', X2AM()->x2_plugin_activation_tab_key, array( $this, 'config_page')
		);
		add_action( 'admin_print_styles-' . $page, array( $this, 'css_scripts' ) );
	}

	// Draw option page
	public function config_page() {

		$settings_tabs = array( X2AM()->x2_plugin_activation_tab_key => __( X2AM()->x2_plugin_menu_tab_activation_title, 'x2pp' ), X2AM()->x2_plugin_deactivation_tab_key => __( X2AM()->x2_plugin_menu_tab_deactivation_title, 'x2pp' ) );
		$current_tab = isset( $_GET[ 'tab' ] ) ? $_GET[ 'tab' ] : X2AM()->x2_plugin_activation_tab_key;
		$tab = isset( $_GET[ 'tab' ] ) ? $_GET[ 'tab' ] : X2AM()->x2_plugin_activation_tab_key;
		?>
		<div class='wrap'>
			<?php screen_icon(); ?>
			<h2><?php _e( X2AM()->x2_plugin_settings_title, 'x2pp' ); ?></h2>

			<h2 class="nav-tab-wrapper">
			<?php
				foreach ( $settings_tabs as $tab_page => $tab_name ) {
					$active_tab = $current_tab == $tab_page ? 'nav-tab-active' : '';
					echo '<a class="nav-tab ' . $active_tab . '" href="?page=' . X2AM()->x2_plugin_activation_tab_key . '&tab=' . $tab_page . '">' . $tab_name . '</a>';
				}
			?>
			</h2>
				<form action='options.php' method='post'>
					<div class="main">
				<?php
					if( $tab == X2AM()->x2_plugin_activation_tab_key ) {
							settings_fields( X2AM()->x2_plugin_data_key );
							do_settings_sections( X2AM()->x2_plugin_activation_tab_key );
							submit_button( __( 'Save Changes', 'x2pp' ) );
					} else {
							settings_fields( X2AM()->x2_plugin_deactivate_checkbox );
							do_settings_sections( X2AM()->x2_plugin_deactivation_tab_key );
							submit_button( __( 'Save Changes', 'x2pp' ) );
					}
				?>
					</div>
				</form>
			</div>
			<?php
	}

	// Register settings
	public function load_settings() {

		register_setting( X2AM()->x2_plugin_data_key, X2AM()->x2_plugin_data_key, array( $this, 'validate_options' ) );

		// API Key
		add_settings_section( X2AM()->x2_plugin_api_key, __( 'X2 API License Activation', 'x2pp' ), array( $this, 'wc_am_api_key_text' ), X2AM()->x2_plugin_activation_tab_key );
		add_settings_field( X2AM()->x2_plugin_api_key, __( 'API License Key', 'x2pp' ), array( $this, 'wc_am_api_key_field' ), X2AM()->x2_plugin_activation_tab_key, X2AM()->x2_plugin_api_key );
		add_settings_field( X2AM()->x2_plugin_activation_email, __( 'API License Email', 'x2pp' ), array( $this, 'wc_am_api_email_field' ), X2AM()->x2_plugin_activation_tab_key, X2AM()->x2_plugin_api_key );

		// Activation settings
		register_setting( X2AM()->x2_plugin_deactivate_checkbox, X2AM()->x2_plugin_deactivate_checkbox, array( $this, 'wc_am_license_key_deactivation' ) );
		add_settings_section( 'deactivate_button', __( 'X2 API License Deactivation', 'x2pp' ), array( $this, 'wc_am_deactivate_text' ), X2AM()->x2_plugin_deactivation_tab_key );
		add_settings_field( 'deactivate_button', __( 'Deactivate API License Key', 'x2pp' ), array( $this, 'wc_am_deactivate_textarea' ), X2AM()->x2_plugin_deactivation_tab_key, 'deactivate_button' );

	}

	// Provides text for api key section
	public function wc_am_api_key_text() {
		//
	}

	// Outputs API License text field
	public function wc_am_api_key_field() {

		echo "<input id='api_key' name='" . X2AM()->x2_plugin_data_key . "[" . X2AM()->x2_plugin_api_key ."]' size='25' type='text' value='" . X2AM()->x2_plugin_options[X2AM()->x2_plugin_api_key] . "' />";
		if ( X2AM()->x2_plugin_options[X2AM()->x2_plugin_api_key] ) {
			echo "<span class='icon-pos'><img src='" . X2AM()->plugin_url() . "includes/resources/api-manager/am/assets/images/complete.png' title='' style='padding-bottom: 4px; vertical-align: middle; margin-right:3px;' /></span>";
		} else {
			echo "<span class='icon-pos'><img src='" . X2AM()->plugin_url() . "includes/resources/api-manager/am/assets/images/warn.png' title='' style='padding-bottom: 4px; vertical-align: middle; margin-right:3px;' /></span>";
		}
	}

	// Outputs API License email text field
	public function wc_am_api_email_field() {

		echo "<input id='activation_email' name='" . X2AM()->x2_plugin_data_key . "[" . X2AM()->x2_plugin_activation_email ."]' size='25' type='text' value='" . X2AM()->x2_plugin_options[X2AM()->x2_plugin_activation_email] . "' />";
		if ( X2AM()->x2_plugin_options[X2AM()->x2_plugin_activation_email] ) {
			echo "<span class='icon-pos'><img src='" . X2AM()->plugin_url() . "includes/resources/api-manager/am/assets/images/complete.png' title='' style='padding-bottom: 4px; vertical-align: middle; margin-right:3px;' /></span>";
		} else {
			echo "<span class='icon-pos'><img src='" . X2AM()->plugin_url() . "includes/resources/api-manager/am/assets/images/warn.png' title='' style='padding-bottom: 4px; vertical-align: middle; margin-right:3px;' /></span>";
		}
	}

	// Sanitizes and validates all input and output for Dashboard
	public function validate_options( $input ) {

		// Load existing options, validate, and update with changes from input before returning
		$options = X2AM()->x2_plugin_options;

		$options[X2AM()->x2_plugin_api_key] = trim( $input[X2AM()->x2_plugin_api_key] );
		$options[X2AM()->x2_plugin_activation_email] = trim( $input[X2AM()->x2_plugin_activation_email] );

		/**
		  * Plugin Activation
		  */
		$api_email = trim( $input[X2AM()->x2_plugin_activation_email] );
		$api_key = trim( $input[X2AM()->x2_plugin_api_key] );

		$activation_status = get_option( X2AM()->x2_plugin_activated_key );
		$checkbox_status = get_option( X2AM()->x2_plugin_deactivate_checkbox );

		$current_api_key = X2AM()->x2_plugin_options[X2AM()->x2_plugin_api_key];

		// Should match the settings_fields() value
		if ( $_REQUEST['option_page'] != X2AM()->x2_plugin_deactivate_checkbox ) {

			if ( $activation_status == 'Deactivated' || $activation_status == '' || $api_key == '' || $api_email == '' || $checkbox_status == 'on' || $current_api_key != $api_key  ) {

				/**
				 * If this is a new key, and an existing key already exists in the database,
				 * deactivate the existing key before activating the new key.
				 */
				if ( $current_api_key != $api_key )
					$this->replace_license_key( $current_api_key );

				$args = array(
					'email' => $api_email,
					'licence_key' => $api_key,
					);

				$activate_results = $this->x2_api_manager_key->activate( $args );

				$activate_results = json_decode( $activate_results, true );

				if ( $activate_results['activated'] == true ) {
					add_settings_error( 'activate_text', 'activate_msg', __( 'Plugin activated. ', 'x2pp' ) . "{$activate_results['message']}.", 'updated' );
					update_option( X2AM()->x2_plugin_activated_key, 'Activated' );
					update_option( X2AM()->x2_plugin_deactivate_checkbox, 'off' );
				}

				if ( $activate_results == false ) {
					add_settings_error( 'api_key_check_text', 'api_key_check_error', __( 'Connection failed to the License Key API server. Try again later.', 'x2pp' ), 'error' );
					$options[X2AM()->x2_plugin_api_key] = '';
					$options[X2AM()->x2_plugin_activation_email] = '';
					update_option( X2AM()->x2_plugin_options[X2AM()->x2_plugin_activated_key], 'Deactivated' );
				}

				if ( isset( $activate_results['code'] ) ) {

					switch ( $activate_results['code'] ) {
						case '100':
							add_settings_error( 'api_email_text', 'api_email_error', "{$activate_results['error']}. {$activate_results['additional info']}", 'error' );
							$options[X2AM()->x2_plugin_activation_email] = '';
							$options[X2AM()->x2_plugin_api_key] = '';
							update_option( X2AM()->x2_plugin_options[X2AM()->x2_plugin_activated_key], 'Deactivated' );
						break;
						case '101':
							add_settings_error( 'api_key_text', 'api_key_error', "{$activate_results['error']}. {$activate_results['additional info']}", 'error' );
							$options[X2AM()->x2_plugin_api_key] = '';
							$options[X2AM()->x2_plugin_activation_email] = '';
							update_option( X2AM()->x2_plugin_options[X2AM()->x2_plugin_activated_key], 'Deactivated' );
						break;
						case '102':
							add_settings_error( 'api_key_purchase_incomplete_text', 'api_key_purchase_incomplete_error', "{$activate_results['error']}. {$activate_results['additional info']}", 'error' );
							$options[X2AM()->x2_plugin_api_key] = '';
							$options[X2AM()->x2_plugin_activation_email] = '';
							update_option( X2AM()->x2_plugin_options[X2AM()->x2_plugin_activated_key], 'Deactivated' );
						break;
						case '103':
								add_settings_error( 'api_key_exceeded_text', 'api_key_exceeded_error', "{$activate_results['error']}. {$activate_results['additional info']}", 'error' );
								$options[X2AM()->x2_plugin_api_key] = '';
								$options[X2AM()->x2_plugin_activation_email] = '';
								update_option( X2AM()->x2_plugin_options[X2AM()->x2_plugin_activated_key], 'Deactivated' );
						break;
						case '104':
								add_settings_error( 'api_key_not_activated_text', 'api_key_not_activated_error', "{$activate_results['error']}. {$activate_results['additional info']}", 'error' );
								$options[X2AM()->x2_plugin_api_key] = '';
								$options[X2AM()->x2_plugin_activation_email] = '';
								update_option( X2AM()->x2_plugin_options[X2AM()->x2_plugin_activated_key], 'Deactivated' );
						break;
						case '105':
								add_settings_error( 'api_key_invalid_text', 'api_key_invalid_error', "{$activate_results['error']}. {$activate_results['additional info']}", 'error' );
								$options[X2AM()->x2_plugin_api_key] = '';
								$options[X2AM()->x2_plugin_activation_email] = '';
								update_option( X2AM()->x2_plugin_options[X2AM()->x2_plugin_activated_key], 'Deactivated' );
						break;
						case '106':
								add_settings_error( 'sub_not_active_text', 'sub_not_active_error', "{$activate_results['error']}. {$activate_results['additional info']}", 'error' );
								$options[X2AM()->x2_plugin_api_key] = '';
								$options[X2AM()->x2_plugin_activation_email] = '';
								update_option( X2AM()->x2_plugin_options[X2AM()->x2_plugin_activated_key], 'Deactivated' );
						break;
					}

				}

			} // End Plugin Activation

		}

		return $options;
	}

	// Deactivate the current license key before activating the new license key
	public function replace_license_key( $current_api_key ) {

		$args = array(
			'email' => X2AM()->x2_plugin_options[X2AM()->x2_plugin_activation_email],
			'licence_key' => $current_api_key,
			);

		$reset = $this->x2_api_manager_key->deactivate( $args ); // reset license key activation

		if ( $reset == true )
			return true;

		return add_settings_error( 'not_deactivated_text', 'not_deactivated_error', __( 'The license could not be deactivated. Use the License Deactivation tab to manually deactivate the license before activating a new license.', 'x2pp' ), 'updated' );
	}

	// Deactivates the license key to allow key to be used on another blog
	public function wc_am_license_key_deactivation( $input ) {

		$activation_status = get_option( X2AM()->x2_plugin_activated_key );

		$args = array(
			'email' => X2AM()->x2_plugin_options[X2AM()->x2_plugin_activation_email],
			'licence_key' => X2AM()->x2_plugin_options[X2AM()->x2_plugin_api_key],
			);

		$options = ( $input == 'on' ? 'on' : 'off' );

		if ( $options == 'on' && $activation_status == 'Activated' && X2AM()->x2_plugin_options[X2AM()->x2_plugin_api_key] != '' && X2AM()->x2_plugin_options[X2AM()->x2_plugin_activation_email] != '' ) {
			$reset = $this->x2_api_manager_key->deactivate( $args ); // reset license key activation

			if ( $reset == true ) {
				$update = array(
					X2AM()->x2_plugin_api_key => '',
					X2AM()->x2_plugin_activation_email => ''
					);
				$merge_options = array_merge( X2AM()->x2_plugin_options, $update );

				update_option( X2AM()->x2_plugin_data_key, $merge_options );

				update_option( X2AM()->x2_plugin_activated_key, 'Deactivated' );

				add_settings_error( 'wc_am_deactivate_text', 'deactivate_msg', __( 'Plugin license deactivated.', 'x2pp' ), 'updated' );

				return $options;
			}

		} else {

			return $options;
		}

	}

	public function wc_am_deactivate_text() {
	}

	public function wc_am_deactivate_textarea() {

		echo '<input type="checkbox" id="' . X2AM()->x2_plugin_deactivate_checkbox . '" name="' . X2AM()->x2_plugin_deactivate_checkbox . '" value="on"';
		echo checked( get_option( X2AM()->x2_plugin_deactivate_checkbox ), 'on' );
		echo '/>';
		?><span class="description"><?php _e( 'Deactivates an API License Key so it can be used on another blog.', 'x2pp' ); ?></span>
		<?php
	}

	// Loads admin style sheets
	public function css_scripts() {

		wp_register_style( X2AM()->x2_plugin_data_key . '-css', X2AM()->plugin_url() . 'includes/resources/api-manager/am/assets/css/admin-settings.css', array(), X2AM()->version, 'all');
		wp_enqueue_style( X2AM()->x2_plugin_data_key . '-css' );
	}

}

new X2_API_Manager_MENU();
