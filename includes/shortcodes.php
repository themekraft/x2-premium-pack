<?php
/**
 * Register all shortcodes.
 */
add_action( 'init', 'x2_register_shortcodes' );
function x2_register_shortcodes() {
	add_shortcode( 'x2_slideshow',              'jquery_slider' );
	add_shortcode( 'x2_facebook_like',          'fb_like' );
	add_shortcode( 'x2_blockquote_left',        'blockquote_left' );
	add_shortcode( 'x2_blockquote_right',       'blockquote_right' );
	add_shortcode( 'x2_button',                 'button' );
	add_shortcode( 'x2_break',                  'horline' );
	add_shortcode( 'x2_clear',                  'clear' );
	add_shortcode( 'x2_col_end',                'col_end' );
	add_shortcode( 'x2_half_col_left',          'half_col_left' );
	add_shortcode( 'x2_half_col_right',         'half_col_right' );
	add_shortcode( 'x2_third_col',              'third_col' );
	add_shortcode( 'x2_third_col_right',        'third_col_right' );
	add_shortcode( 'x2_fourth_col',             'fourth_col' );
	add_shortcode( 'x2_fourth_col_right',       'fourth_col_right' );
	add_shortcode( 'x2_three_fourth_col',       'three_fourth_col' );
	add_shortcode( 'x2_three_fourth_col_right', 'three_fourth_col_right' );
	add_shortcode( 'x2_list_posts',             'x2_list_posts' );
	add_shortcode( 'x2_list_carousel',          'x2_carousel_loop' );
	add_shortcode( 'x2_img_effect',             'img_effect' );
	add_shortcode( 'x2_accordion_start',        'accordion_start' );
	add_shortcode( 'x2_accordion_end',          'accordion_end' );
	add_shortcode( 'x2_a_content_start',        'accordion_content_start' );
	add_shortcode( 'x2_a_content_end',          'accordion_content_end' );
	add_shortcode( 'x2_empty',                  'nothing' );
}

// shortcode facebook like button
function fb_like() {
	$pageURL = 'http';

	if (!empty($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {$pageURL .= "s";}

	$pageURL .= "://";

	if ($_SERVER["SERVER_PORT"] != "80") {
		$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
	} else {
		$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	}

	$tmp = '<iframe src="http://www.facebook.com/plugins/like.php?href='.$pageURL.'&layout=standard&show_faces=true&width=450&action=like&colorscheme=light" scrolling="no" frameborder="0" allowTransparency="true" style="border:none; overflow:hidden; width:auto; height:60px"></iframe>';

	return $tmp;
}

// blockquote_left = add a quotation, left floated
function blockquote_left($atts,$content = null) {
	return '<span class="x2_blockquote x2_blockquote_left">"'.$content.'"</span>';
}

// blockquote_right = add a quotation, right floated
function blockquote_right($atts,$content = null) {
	return '<span class="x2_blockquote x2_blockquote_right">"'.$content.'"</span>';
}

// button = add a button with custom text and link
function button($atts,$content = null) {
	extract(shortcode_atts(array(
		'link'   => '#',
		'target' => ''
	), $atts));
	return '<a href="' . esc_url( $link ) . '" target="' . $target . '" class="btn">' . $content . '</a>';
}

// deprecated for CC - break = horizontal line / enter
function horline($atts,$content = null) {
	return '<br />';
}

// clear = reset all css from the elements before
function clear($atts,$content = null) {
	return '<div class="clear"></div>';
}

// col_end = end of a column shortcode for advanced use (hierarchical mode)
function col_end(){
	return '</div>';
}

// half_col_left = half column, left floated
function half_col_left($atts,$content = null) {
	extract(shortcode_atts(array(
		'background_color' => 'none',
		'border_color'     => 'transparent',
		'radius'           => '0',
		'shadow_color'     => 'transparent',
		'height'           => 'auto',
		'background_image' => 'none',
		'hierarchical'     => 'off',
	), $atts));

	$radius = absint( $radius );

	if($height != 'auto'){ $height = $height.'px'; }
	if($background_color != 'none'){ $background_color = '#'.$background_color; }
	if($border_color != 'transparent'){ $border_color = '#'.$border_color; }
	if($shadow_color != 'transparent'){ $shadow_color = '#'.$shadow_color; }

	$add='';
	if($background_color !='none' || $border_color !='transparent' || $shadow_color !='transparent' || $background_image !='none') { $add='padding:2%; width:44%;'; }
	$add_bg='';
	if($background_image !='none') { $add_bg='background-image:url('.$background_image.');'; }
	$tmp = '<div class="half_col_left"
				style="background:'.$background_color.';
						border: 1px solid; border-color:'.$border_color.';
						-moz-border-radius:'.$radius.'px;
						-webkit-border-radius:'.$radius.'px;
						border-radius:'.$radius.'px;
						-moz-box-shadow: 2px 2px 2px '.$shadow_color.';
						-webkit-box-shadow: 2px 2px 2px '.$shadow_color.';
						box-shadow: 2px 2px 2px '.$shadow_color.';'.$add_bg.'
						height:'.$height.';'.$add.'">';
	if($hierarchical == 'off'){
		$tmp .= $content;
		$tmp .= '</div>';
	}
	return $tmp;
}

// half_col_right = half column, right floated
function half_col_right($atts,$content = null) {
	extract(shortcode_atts(array(
		'background_color' => 'none',
		'border_color'     => 'transparent',
		'radius'           => '0',
		'shadow_color'     => 'transparent',
		'height'           => 'auto',
		'background_image' => 'none',
		'hierarchical'     => 'off',
	), $atts));

	$radius = absint( $radius );

	if($height != 'auto'){ $height = $height.'px'; }
	if($background_color != 'none'){ $background_color = '#'.$background_color; }
	if($border_color != 'transparent'){ $border_color = '#'.$border_color; }
	if($shadow_color != 'transparent'){ $shadow_color = '#'.$shadow_color; }

	$add='';
	if($background_color !='none' || $border_color !='transparent' || $shadow_color !='transparent' || $background_image !='none') { $add='padding:2%; width:44%;'; }
	$add_bg='';
	if($background_image !='none') { $add_bg='background-image:url('.$background_image.');'; }
	$tmp = '<div class="half_col_right"
				style="background:'.$background_color.';
						border: 1px solid; border-color:'.$border_color.';
						-moz-border-radius:'.$radius.'px;
						-webkit-border-radius:'.$radius.'px;
						border-radius:'.$radius.'px;
						-moz-box-shadow: 2px 2px 2px '.$shadow_color.';
						-webkit-box-shadow: 2px 2px 2px '.$shadow_color.';
						box-shadow: 2px 2px 2px '.$shadow_color.';'.$add_bg.'
						height:'.$height.';'.$add.'">';
	if($hierarchical == 'off'){
		$tmp .= $content;
		$tmp .= '</div><div class="clear"></div>';
	}
	return $tmp;

}

// third_col = one third column, left floated
function third_col($atts,$content = null) {
	extract(shortcode_atts(array(
		'background_color' => 'none',
		'border_color'     => 'transparent',
		'radius'           => '0',
		'shadow_color'     => 'transparent',
		'height'           => 'auto',
		'background_image' => 'none',
		'hierarchical'     => 'off',
	), $atts));

	if($height != 'auto'){ $height = $height.'px'; }
	if($background_color != 'none'){ $background_color = '#'.$background_color; }
	if($border_color != 'transparent'){ $border_color = '#'.$border_color; }
	if($shadow_color != 'transparent'){ $shadow_color = '#'.$shadow_color; }

	$add='';
	if($background_color !='none' || $border_color !='transparent' || $shadow_color !='transparent' || $background_image !='none') { $add='padding:2%; width:26.7%;'; }
	$addborder='';
	if($border_color !='transparent') { $addborder ='border:1px solid '.$border_color.'; margin-right:2.7%;'; }
	$add_bg='';
	if($background_image !='none') { $add_bg='background-image:url('.$background_image.');'; }
	$tmp = '<div class="third_col"
				style="background:'.$background_color.';'.$addborder.'
						-moz-border-radius:'.$radius.'px;
						-webkit-border-radius:'.$radius.'px;
						border-radius:'.$radius.'px;
						-moz-box-shadow: 2px 2px 2px '.$shadow_color.';
						-webkit-box-shadow: 2px 2px 2px '.$shadow_color.';
						box-shadow: 2px 2px 2px '.$shadow_color.';'.$add_bg.'
						height:'.$height.';'.$add.'">';
	if($hierarchical == 'off'){
		$tmp .= $content;
		$tmp .= '</div>';
	}
	return $tmp;
}

// third_col_right = one third column, right floated
function third_col_right($atts,$content = null) {
	extract(shortcode_atts(array(
		'background_color' => 'none',
		'border_color'     => 'transparent',
		'radius'           => '0',
		'shadow_color'     => 'transparent',
		'height'           => 'auto',
		'background_image' => 'none',
		'hierarchical'     => 'off',
	), $atts));

	if($height != 'auto'){ $height = $height.'px'; }
	if($background_color != 'none'){ $background_color = '#'.$background_color; }
	if($border_color != 'transparent'){ $border_color = '#'.$border_color; }
	if($shadow_color != 'transparent'){ $shadow_color = '#'.$shadow_color; }

	$add='';
	if($background_color !='none' || $border_color !='transparent' || $shadow_color !='transparent' || $background_image !='none') { $add='padding:2%; width:27%;'; }
	$addborder='';
	if($border_color !='transparent') { $addborder ='border:1px solid '.$border_color.';'; }
	$add_bg='';
	if($background_image !='none') { $add_bg='background-image:url('.$background_image.');'; }
	$tmp = '<div class="third_col_right"
				style="background:'.$background_color.';'.$addborder.'
						-moz-border-radius:'.$radius.'px;
						-webkit-border-radius:'.$radius.'px;
						border-radius:'.$radius.'px;
						-moz-box-shadow: 2px 2px 2px '.$shadow_color.';
						-webkit-box-shadow: 2px 2px 2px '.$shadow_color.';
						box-shadow: 2px 2px 2px '.$shadow_color.';'.$add_bg.'
						height:'.$height.';'.$add.'">';
	if($hierarchical == 'off'){
		$tmp .= $content;
		$tmp .= '</div><div class="clear"></div>';
	}
	return $tmp;
}

// [fourth_col = one fourth column, left floated]
function fourth_col($atts,$content = null) {
	extract(shortcode_atts(array(
		'background_color' => 'none',
		'border_color'     => 'transparent',
		'radius'           => '0',
		'shadow_color'     => 'transparent',
		'height'           => 'auto',
		'background_image' => 'none',
		'hierarchical'     => 'off',
	), $atts));

	if($height != 'auto'){ $height = $height.'px'; }
	if($background_color != 'none'){ $background_color = '#'.$background_color; }
	if($border_color != 'transparent'){ $border_color = '#'.$border_color; }
	if($shadow_color != 'transparent'){ $shadow_color = '#'.$shadow_color; }

	$add='';
	if($background_color !='none' || $border_color !='transparent' || $shadow_color !='transparent' || $background_image !='none') { $add='padding:2%; width:18.5%;'; }
	$addborder='';
	if($border_color !='transparent') { $addborder ='border:1px solid '.$border_color.'; margin-right:2.7%;'; }
	$add_bg='';
	if($background_image !='none') { $add_bg='background-image:url('.$background_image.');'; }
	$tmp = '<div class="fourth_col"
				style="background:'.$background_color.';'.$addborder.'
						-moz-border-radius:'.$radius.'px;
						-webkit-border-radius:'.$radius.'px;
						border-radius:'.$radius.'px;
						-moz-box-shadow: 2px 2px 2px '.$shadow_color.';
						-webkit-box-shadow: 2px 2px 2px '.$shadow_color.';
						box-shadow: 2px 2px 2px '.$shadow_color.';'.$add_bg.'
						height:'.$height.';'.$add.'">';
	if($hierarchical == 'off'){
		$tmp .= $content;
		$tmp .= '</div>';
	}
	return $tmp;
}

// [fourth_col_right = one fourth column, right floated]
function fourth_col_right($atts,$content = null) {
	extract(shortcode_atts(array(
		'background_color' => 'none',
		'border_color'     => 'transparent',
		'radius'           => '0',
		'shadow_color'     => 'transparent',
		'height'           => 'auto',
		'background_image' => 'none',
		'hierarchical'     => 'off',
	), $atts));

	if($height != 'auto'){ $height = $height.'px'; }
	if($background_color != 'none'){ $background_color = '#'.$background_color; }
	if($border_color != 'transparent'){ $border_color = '#'.$border_color; }
	if($shadow_color != 'transparent'){ $shadow_color = '#'.$shadow_color; }

	$add='';
	if($background_color !='none' || $border_color !='transparent' || $shadow_color !='transparent' || $background_image !='none') { $add='padding:2%; width:18.5%;'; }
	$add_border='';
	if($border_color !='transparent') { $add_border ='border:1px solid '.$border_color.';'; }
	$add_bg='';
	if($background_image !='none') { $add_bg='background-image:url('.$background_image.');'; }
	$tmp = '<div class="fourth_col_right"
				style="background:'.$background_color.';'.$add_border.'
						-moz-border-radius:'.$radius.'px;
						-webkit-border-radius:'.$radius.'px;
						border-radius:'.$radius.'px;
						-moz-box-shadow: 2px 2px 2px '.$shadow_color.';
						-webkit-box-shadow: 2px 2px 2px '.$shadow_color.';
						box-shadow: 2px 2px 2px '.$shadow_color.';'.$add_bg.'
						height:'.$height.';'.$add.'">';
	if($hierarchical == 'off'){
		$tmp .= $content;
		$tmp .= '</div><div class="clear"></div>';
	}
	return $tmp;
}

// [three_fourth_col = three fourth column, left floated]
function three_fourth_col($atts,$content = null) {
	extract(shortcode_atts(array(
		'background_color' => 'none',
		'border_color'     => 'transparent',
		'radius'           => '0',
		'shadow_color'     => 'transparent',
		'height'           => 'auto',
		'background_image' => 'none',
		'hierarchical'     => 'off',
	), $atts));

	if($height != 'auto'){ $height = $height.'px'; }
	if($background_color != 'none'){ $background_color = '#'.$background_color; }
	if($border_color != 'transparent'){ $border_color = '#'.$border_color; }
	if($shadow_color != 'transparent'){ $shadow_color = '#'.$shadow_color; }

	$add='';
	if($background_color !='none' || $border_color !='transparent' || $shadow_color !='transparent' || $background_image !='none') { $add='padding:2%; width:18.5%;'; }
	$addborder='';
	if($border_color !='transparent') { $addborder ='border:1px solid '.$border_color.'; margin-right:2.7%;'; }
	$add_bg='';
	if($background_image !='none') { $add_bg='background-image:url('.$background_image.');'; }
	$tmp = '<div class="three_fourth_col"
				style="background:'.$background_color.';'.$addborder.'
						-moz-border-radius:'.$radius.'px;
						-webkit-border-radius:'.$radius.'px;
						border-radius:'.$radius.'px;
						-moz-box-shadow: 2px 2px 2px '.$shadow_color.';
						-webkit-box-shadow: 2px 2px 2px '.$shadow_color.';
						box-shadow: 2px 2px 2px '.$shadow_color.';'.$add_bg.'
						height:'.$height.';'.$add.'">';
	if($hierarchical == 'off'){
		$tmp .= $content;
		$tmp .= '</div>';
	}
	return $tmp;
}

// [three_fourth_col_right = three fourth column, right floated]
function three_fourth_col_right($atts,$content = null) {
	extract(shortcode_atts(array(
		'background_color' => 'none',
		'border_color'     => 'transparent',
		'radius'           => '0',
		'shadow_color'     => 'transparent',
		'height'           => 'auto',
		'background_image' => 'none',
		'hierarchical'     => 'off',
	), $atts));
	if($height != 'auto'){ $height = $height.'px'; }
	if($background_color != 'none'){ $background_color = '#'.$background_color; }
	if($border_color != 'transparent'){ $border_color = '#'.$border_color; }
	if($shadow_color != 'transparent'){ $shadow_color = '#'.$shadow_color; }

	$add='';
	if($background_color !='none' || $border_color !='transparent' || $shadow_color !='transparent' || $background_image !='none') { $add='padding:2%; width:18.5%;'; }
	$add_border='';
	if($border_color !='transparent') { $add_border ='border:1px solid '.$border_color.';'; }
	$add_bg='';
	if($background_image !='none') { $add_bg='background-image:url('.$background_image.');'; }
	$tmp = '<div class="three_fourth_col_right"
				style="background:'.$background_color.';'.$add_border.'
						-moz-border-radius:'.$radius.'px;
						-webkit-border-radius:'.$radius.'px;
						border-radius:'.$radius.'px;
						-moz-box-shadow: 2px 2px 2px '.$shadow_color.';
						-webkit-box-shadow: 2px 2px 2px '.$shadow_color.';
						box-shadow: 2px 2px 2px '.$shadow_color.';'.$add_bg.'
						height:'.$height.';'.$add.'">';
	if($hierarchical == 'off'){
		$tmp .= $content;
		$tmp .= '</div><div class="clear"></div>';
	}
	return $tmp;
}

// [image effect]
function img_effect($atts,$content = null) {
	global $cap, $x2_js;

	switch ($cap->style_css){
		case 'dark':
			$img_border_color = '363636';
			$img_shadow_color = '393939';
			$frx2_plugin_color      = '333333';
			$f_border_color   = '363636';
			$f_shadow_color   = 'transparent';
			break;
		case 'natural':
			$img_border_color = 'F5E5B3';
			$img_shadow_color = 'F5E5B3';
			$frx2_plugin_color      = 'F5E5B3';
			$f_border_color   = 'd0b971';
			$f_shadow_color   = 'ccb56d';
			break;
		case 'white':
			$img_border_color = 'e6e6e6';
			$img_shadow_color = 'e3e3e3';
			$frx2_plugin_color      = 'f1f1f1';
			$f_border_color   = 'd3d3d3';
			$f_shadow_color   = 'cccccc';
			break;
		case 'light':
			$img_border_color = 'e6e6e6';
			$img_shadow_color = 'e3e3e3';
			$frx2_plugin_color      = 'f1f1f1';
			$f_border_color   = 'd3d3d3';
			$f_shadow_color   = 'cccccc';
			break;
		case 'grey':
			$img_border_color = 'e6e6e6';
			$img_shadow_color = 'e3e3e3';
			$frx2_plugin_color      = 'f1f1f1';
			$f_border_color   = 'd3d3d3';
			$f_shadow_color   = 'cccccc';
			break;
		case 'black':
			$img_border_color = '363636';
			$img_shadow_color = '393939';
			$frx2_plugin_color      = '333333';
			$f_border_color   = '363636';
			$f_shadow_color   = 'transparent';
			break;
		default:
			$img_border_color = 'e6e6e6';
			$img_shadow_color = 'e3e3e3';
			$frx2_plugin_color      = 'f1f1f1';
			$f_border_color   = 'd3d3d3';
			$f_shadow_color   = 'cccccc';
			break;
	}

	if($cap->bg_body_color){
		$frx2_plugin_color = $cap->bg_body_color;
	}

	extract(shortcode_atts(array(
		'url'               => '',
		'alt'               => '',
		'title'             => '',
		'width'             => '',
		'height'            => '',
		'ropacity'          => '0.5',
		'rheight'           => '0.25',
		'id'                => '',
		'rspace'            => '1',
		'reflection'        => 'off',
		'img_border_color'  => $img_border_color,
		'img_shadow_color'  => $img_shadow_color,
		'frame'             => 'off',
		'frx2_plugin_width' => '10',
		'frx2_plugin_color' => $frx2_plugin_color,
		'f_border_color'    => $f_border_color,
		'f_shadow_color'    => $f_shadow_color,
		'img_css'           => '',
		'frx2_plugin_css'   => '',
		'radius'            => '0',
		'float'             => 'none'
	), $atts));

	$radius = absint( $radius );

	$tmp .= '<style type="text/css">';
	$tmp .= 'div.post img {';
	$tmp .= 'margin: 0px;';
	$tmp .= '}';

	$tmp .= 'div.post img.rspace'.$id.', div.page img.rspace'.$id.'  {';
	if($frame == 'off'){
		$tmp .= 'float:'.$float.';';
	}
	$tmp .= '-moz-box-shadow: 1px 1px 4px #'.$img_shadow_color.';';
	$tmp .= '-webkit-box-shadow: 1px 1px 4px #'.$img_shadow_color.';';
	$tmp .= 'box-shadow: 1px 1px 4px #'.$img_shadow_color.';';
	$tmp .= 'border-color: #'.$img_border_color.' !important;';
	if($frame != 'off') {
		$tmp .= 'margin: 0;';
	} else {
		if($reflection != 'off') {
			$tmp .= 'margin: 0 0 20px 0;';
		} else {
			$tmp .= 'margin: 0 20px 20px 0;';
		}
	}
	$tmp .= 'border-style: solid !important;';
	$tmp .= 'border-width: 1px !important;';
	$tmp .= '}';

	$tmp .= 'div.rspace'.$id.' img { margin-bottom: '.$rspace.'px !important;';
	if($frame == 'on' && $float == 'none') {
		$tmp .= 'float:left;';
	} else {
		$tmp .= 'float:'.$float.';';
	}
	$tmp .= '}';

	$tmp .= '#img_frame'.$id.'{';
	if($frame == 'on' && $float == 'none') {
		$tmp .= 'float:left;';
	} else {
		$tmp .= 'float:'.$float.';';
	}
	$tmp .= 'padding:'.$frx2_plugin_width.'px;';
	$tmp .= '-moz-box-shadow: 2px 2px 3px #'.$f_shadow_color.';';
	$tmp .= '-webkit-box-shadow: 2px 2px 3px #'.$f_shadow_color.';';
	$tmp .= 'box-shadow: 2px 2px 3px #'.$f_shadow_color.';';
	$tmp .= '-moz-border-radius:'.$radius.'px;';
	$tmp .= '-webkit-border-radius:'.$radius.'px;';
	$tmp .= 'border-radius:'.$radius.'px;';
	$tmp .= 'background: none repeat scroll 0 0 #'.$frx2_plugin_color.';';
	$tmp .= 'border-color: #'.$f_border_color.';';
	$tmp .= 'border-style: solid !important;';
	$tmp .= 'border-width: 1px !important;';
	$tmp .= 'margin: 0 20px 20px 0;';
	$tmp .= '}';
	$tmp .= '</style>';

	if ($reflection != 'off'){
		$x2_js['img_effect'][] = array(
									'id'       => $id,
									'rheight'  => $rheight,
									'ropacity' => $ropacity
								);
	}
	if($frame != 'off'){
		$tmp .= '<div id="img_frame'.$id.'" style="'.$frx2_plugin_css.'">';
	}
	$tmp .= '<img src="'.esc_url( $url ).'" alt="'.esc_attr( $alt ).'" title="'.esc_attr( $title ).'" width="'.$width.'" height="'.$height.'" id="img_effect'.$id.'" class="rspace'.$id.'" style="'.$img_css.'" />';
	if($frame != 'off'){
		$tmp .= '<br />'.$content;
		$tmp .= '</div>';
	}
	if($float == 'none'){
		$tmp .= '<div class="clear"></div>';
	}

	return $tmp;
}

// [Accordion start]
function accordion_start($atts) {
	global $cap, $x2_js;

	switch ($cap->style_css){
		case 'dark':
			$title_background          = '';
			$title_background_selected = '';
			$border_color              = '';
			$content_background_color  = '';
			break;
		case 'natural':
			$title_background          = '';
			$title_background_selected = '';
			$border_color              = '';
			$content_background_color  = '';
			break;
		case 'white':
			$title_background          = '';
			$title_background_selected = '';
			$border_color              = '';
			$content_background_color  = '';
			break;
		case 'light':
			$title_background          = '';
			$title_background_selected = '';
			$border_color              = '';
			$content_background_color  = '';
			break;
		case 'grey':
			$title_background          = '';
			$title_background_selected = '';
			$border_color              = '';
			$content_background_color  = '';
			break;
		case 'black':
			$title_background          = '';
			$title_background_selected = '';
			$border_color              = '';
			$content_background_color  = '';
			break;
		default:
			$title_background          = '';
			$title_background_selected = '';
			$border_color              = '';
			$content_background_color  = '';
			break;
	}

	extract(shortcode_atts(array(
		'id'                        => '',
		'icon_url'                  => '',
		'icon_url_selected'         => '',
		'title_background'          => $title_background,
		'title_background_selected' => $title_background_selected,
		'border_color'              => $border_color,
		'content_background_color'  => $content_background_color,
		'accordion_css'             => '',
		'title_css'                 => '',
	), $atts));

	// inline js
	$x2_js['accordion'][] = array(
								'id' => $id
							);

	if($icon_url != ''){
		$tmp .= '<style type="text/css">';
		$tmp .='#accordion'.$id.' h3 {';
		$tmp .='background: url('.esc_url( $icon_url ).') no-repeat scroll 4px 50%;';
		$tmp .=' }';
		$tmp .= '</style>';
	}

	if($title_background != ''){
		$tmp .= '<style type="text/css">';
		$tmp .='#accordion'.$id.' h3 {';
		$tmp .='background-color: #'.$title_background.';';
		$tmp .=' }';
		$tmp .= '</style>';
	}

	if($border_color != ''){
		$tmp .= '<style type="text/css">';
		$tmp .='#accordion'.$id.' h3 {';
		if($border_color != 'transparent') { $border_color = '#'.$border_color; }
		$tmp .='border-color: '.$border_color.';';
		$tmp .=' }';
		$tmp .='#accordion'.$id.' {';
		$tmp .='border-color: '.$border_color.';';
		$tmp .=' }';
		$tmp .='#accordion'.$id.' div {';
		$tmp .='    border-left: 1px solid '.$border_color.'; border-right: 1px solid '.$border_color.'; }';
		$tmp .= '</style>';

	}

	if($content_background_color != ''){
		$tmp .= '<style type="text/css">';
		$tmp .='#accordion'.$id.' div {';
		$tmp .='background: none repeat scroll 0 0 #'.$content_background_color.';';
		$tmp .=' }';
		$tmp .= '</style>';

	}
	if($icon_url_selected != ''){
		$tmp .= '<style type="text/css">';
		$tmp .='#accordion'.$id.' h3.active {';
		$tmp .='background: url('.esc_url( $icon_url_selected ).') no-repeat scroll 4px 50% ;';
		$tmp .=' }';
		$tmp .= '</style>';
	}

	if($title_background_selected != ''){
		$tmp .= '<style type="text/css">';
		$tmp .='#accordion'.$id.' h3.active {';
		$tmp .='background-color: #'.$title_background_selected.';';
		$tmp .=' }';
		$tmp .='#accordion'.$id.' h3:hover {';
		$tmp .='background-color: #'.$title_background_selected.';';
		$tmp .=' }';
		$tmp .= '</style>';
	}

	if($title_css != ''){
		$tmp .= '<style type="text/css">';
		$tmp .='#accordion'.$id.' h3, #accordion'.$id.' h3.active  {';
		$tmp .=$title_css;
		$tmp .=' }';
		$tmp .= '</style>';
	}

	$tmp .='<div class="accordion" id="accordion'.$id.'" style="'.$accordion_css.'">';
	return $tmp;
}

// [Accordion end]
function accordion_end() {
	return '</div><!-- /.accordion -->';
}

// [Accordion content start]
function accordion_content_start($atts) {
	extract(shortcode_atts(array(
		'id' => '',
	), $atts));

	$tmp .= '<div class="accordion-toggle swap'.$id.'">';
	return $tmp;
}

// [Accordion content end]
// [Accordion end]
function accordion_content_end() {
	return '</div>';
}

// nothing
// empty = just to display shortcodes without execution - needed for demos.
function nothing($atts,$content = null) {
	return $content;
}
?>