jQuery(document).ready(function(){
	jQuery('#copy_child_theme').click(function(){
		var action = jQuery(this);
		
		
		if (confirm('Alright, copy now?'))
		jQuery.ajax({
			type: 'POST',
			url: ajaxurl,
			data: {"action": "x2_xcopy_ajax"},
			success: function(data){
				alert('Child Theme Copied ;)');
			},
			error: function() { 
				alert('Something went wrong.. ;-(sorry)');
			}
		});
	});
});

// zendesk script for the x2 pro plugin
jQuery(document).ready(function(jQuery) {
    if (typeof(Zenbox) !== "undefined") {
        Zenbox.init({
            dropboxID:   "20204291",
            url:         "https://themekraft.zendesk.com",
            tabTooltip:  "Support",
            tabColor:    "black",
            tabPosition: "Left",
            hide_tab: true
        });
    }
});