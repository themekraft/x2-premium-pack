<?php

/**
 *  Carousel posts widget
 *
 * @package x2
 * @since 2.0
 */

class X2_Carousel_Posts_Widget extends WP_Widget {
    function X2_Carousel_Posts_Widget() {
          //Constructor
            parent::WP_Widget(false,
                __('x2 -> Carousel Posts', 'x2pp'),
                array(
                    'description' => __('Carousel Posts', 'x2pp')
                )
            );
    }

    function widget($args, $instance) {
        extract( $args );
        $tmp = '';
        $selected_category = !empty($instance['category']) ? esc_attr($instance['category']) : '';
        $title = empty($instance['title']) ? ' ' : apply_filters('widget_title', $instance['title']);

        $listing_style = !empty($instance['featured_posts_listing_style']) ? $instance['featured_posts_listing_style'] : '';

        $selected_post_type = !empty($instance['featured_posts_post_type']) ? esc_attr($instance['featured_posts_post_type']) : '';

        $featured_posts_order_by = !empty($instance['featured_posts_order_by']) ? $instance['featured_posts_order_by'] : '';
        $featured_posts_order = !empty($instance['featured_posts_order']) ? $instance['featured_posts_order'] : '';
        $featured_posts_show_sticky = !empty($instance['featured_posts_show_sticky']) ?$instance['featured_posts_show_sticky'] : '';
        $featured_posts_show_pages_by_id = !empty($instance['featured_posts_show_pages_by_id']) ? $instance['featured_posts_show_pages_by_id'] : '';
        $featured_posts_amount = !empty($instance['featured_posts_amount']) ? $instance['featured_posts_amount'] : '';
        $featured_posts_widget_height = !empty($instance['featured_posts_widget_height']) ? $instance['featured_posts_widget_height'] : '';

        $featured_posts_widget_autoplay = !empty($instance['featured_posts_widget_autoplay']) ? $instance['featured_posts_widget_autoplay'] : 'off';
        $featured_posts_widget_slideshow_time = !empty($instance['featured_posts_widget_slideshow_time']) ? $instance['featured_posts_widget_slideshow_time'] : '5000';

        $tmp .= '<div style="height:'.$featured_posts_widget_height.'px;">';
        $tmp .= $before_widget;

        if ( ! empty( $title ) )
            $tmp .=  $before_title . $title . $after_title;

        $atts = array(
            'amount'         => $featured_posts_amount,
            'category_name'  => $selected_category,
            'img_position'   => $listing_style,
            'page_id'        => $featured_posts_show_pages_by_id,
            'post_type'      => $selected_post_type,
            'featured_id'    => $widget_id,
            'orderby'        => $featured_posts_order_by,
            'order'          => $featured_posts_order,
            'autoplay'       => $featured_posts_widget_autoplay,
            'slideshow_time' => $featured_posts_widget_slideshow_time,
            'featured_posts_show_sticky' => $featured_posts_show_sticky,
        );



        $tmp .= x2_carousel_loop($atts,$content = null);
    	$tmp .= $after_widget;
        $tmp .= '</div>';

        echo $tmp;
        wp_reset_query();
    }
    function update($new_instance, $old_instance) {
        //update and save the widget
        return $new_instance;
    }
    function form($instance) {

        //widgetform in backend
        $selected_category = !empty($instance['category']) ? esc_attr($instance['category']) : '';
        $selected_post_type = !empty($instance['featured_posts_post_type']) ? esc_attr($instance['featured_posts_post_type']) : '';
        $title = !empty($instance['title']) ? strip_tags($instance['title']) : '';
        $listing_style = !empty($instance['featured_posts_listing_style']) ? esc_attr($instance['featured_posts_listing_style']) : '';


        $featured_posts_order_by = !empty($instance['featured_posts_order_by']) ? $instance['featured_posts_order_by'] : '';
        $featured_posts_order = !empty($instance['featured_posts_order']) ? $instance['featured_posts_order'] : '';
        $featured_posts_show_sticky = !empty($instance['featured_posts_show_sticky']) ? $instance['featured_posts_show_sticky'] : '';
        $featured_posts_show_pages_by_id = !empty($instance['featured_posts_show_pages_by_id'])? $instance['featured_posts_show_pages_by_id'] : '';
        $featured_posts_amount = !empty($instance['featured_posts_amount']) ? $instance['featured_posts_amount'] : '';
        $featured_posts_widget_height = !empty($instance['featured_posts_widget_height']) ? $instance['featured_posts_widget_height'] : '';

        $featured_posts_widget_autoplay = !empty($instance['featured_posts_widget_autoplay']) ? $instance['featured_posts_widget_autoplay'] : 'off';

        $featured_posts_widget_slideshow_time = !empty($instance['featured_posts_widget_slideshow_time']) ? $instance['featured_posts_widget_slideshow_time'] : '5000';

       // Get the existing categories and build a simple select dropdown for the user.

        $args = array('echo' => '0','hide_empty' => '0');
        $categories = get_categories($args);
        $cat_options[] = '<option value="all-categories">'. __('All categories', 'x2pp') .'</option>';
        foreach($categories as $category) {
            $selected = $selected_category === $category->slug ? ' selected="selected"' : '';
            $cat_options[] = '<option value="' . $category->slug .'"' . $selected . '>' . $category->name . '</option>';
        }

        $args=array(
          'public'   => true,
        );
        $output = 'names'; // names or objects, note names is the default
        $operator = 'and'; // 'and' or 'or'
        $post_types=get_post_types($args,$output,$operator);
        foreach ($post_types  as $post_type ) {

            $selected = $selected_post_type === $post_type ? ' selected="selected"' : '';
            $post_type_options[] = '<option value="' . $post_type .'"' . $selected . '>' . $post_type . '</option>';

        }?>

        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'x2pp'); ?> </label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('List posts listing style:', 'x2pp'); ?> </label>
            <select name="<?php echo $this->get_field_name('featured_posts_listing_style'); ?>" id="<?php echo $this->get_field_id('featured_posts_listing_style'); ?>">

            <?php
            $tkf->list_post_template_name = array('carousel','carousel caption', 'caption', "image left","image right","image top","image bottom","image only","widget style - with description", "widget style - no description")
            ?>

        <?php if(is_array($tkf->list_post_template_name)){  foreach ($tkf->list_post_template_name as $key => $value) { ?>
                <option <?php if($listing_style == $value){ ?> selected <?php } ?> value="<?php echo $value ?>"><?php echo $value ?></option>
        <?php } } ?>

             </select>

        </p>

        <p>
            <label for="<?php echo $this->get_field_id('category'); ?>">
                <?php _e('Include category (optional):', 'x2pp'); ?>
            </label>
            <select id="<?php echo $this->get_field_id('category'); ?>" class="widefat" name="<?php echo $this->get_field_name('category'); ?>">
                <?php echo implode('', $cat_options); ?>
            </select>
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('featured_posts_order_by'); ?>"><?php _e('Order by:', 'x2pp'); ?> </label>
            <input class="widefat" id="<?php echo $this->get_field_id('featured_posts_order_by'); ?>" name="<?php echo $this->get_field_name('featured_posts_order_by'); ?>" type="text" value="<?php echo esc_attr($featured_posts_order_by); ?>" />
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('featured_posts_order'); ?>"><?php _e('Order:', 'x2pp'); ?> </label><br />
            <select name="<?php echo $this->get_field_name('featured_posts_order'); ?>" id="<?php echo $this->get_field_id('featured_posts_order'); ?>">
                <option <?php if($featured_posts_order == 'ASC'){ ?> selected <?php } ?> value="ASC"><?php _e('Ascending', 'x2pp'); ?></option>
                <option <?php if($featured_posts_order == 'DESC'){ ?> selected <?php } ?> value="DESC"><?php _e('Descending', 'x2pp'); ?></option>
             </select>
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('featured_posts_show_sticky'); ?>"><?php _e('Show only sticky posts:', 'x2pp'); ?> </label><br />
            <input type="checkbox" id="<?php echo $this->get_field_id('featured_posts_show_sticky'); ?>" name="<?php echo $this->get_field_name('featured_posts_show_sticky'); ?>" value="on" <?php if(esc_attr($featured_posts_show_sticky) == 'on'){ ?> checked="checked" <?php } ?> /><br />
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('featured_posts_post_type'); ?>">
                <?php _e('Include post type (optional):', 'x2pp'); ?>
            </label>
            <select id="<?php echo $this->get_field_id('featured_posts_post_type'); ?>" class="widefat" name="<?php echo $this->get_field_name('featured_posts_post_type'); ?>">
                <?php echo implode('', $post_type_options); ?>
            </select>
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('featured_posts_show_pages_by_id'); ?>"><?php _e('Page IDs:', 'x2pp'); ?> </label>
            <input class="widefat" id="<?php echo $this->get_field_id('featured_posts_show_pages_by_id'); ?>" name="<?php echo $this->get_field_name('featured_posts_show_pages_by_id'); ?>" type="text" value="<?php echo esc_attr($featured_posts_show_pages_by_id); ?>" />
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('featured_posts_amount'); ?>"><?php _e('Amount of posts:', 'x2pp'); ?> </label>
            <input class="widefat" id="<?php echo $this->get_field_id('featured_posts_amount'); ?>" name="<?php echo $this->get_field_name('featured_posts_amount'); ?>" type="text" value="<?php echo esc_attr($featured_posts_amount); ?>" />
        </p>

         <p>
            <label for="<?php echo $this->get_field_id('featured_posts_widget_height'); ?>"><?php _e('Widget height: <br> For the best result of the jQuery effects we recommend to set a fixed height. We can not do this for you, as we do not know how you configure the widget.<br> Just type a number without px', 'x2pp'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('featured_posts_widget_height'); ?>" name="<?php echo $this->get_field_name('featured_posts_widget_height'); ?>" type="text" value="<?php echo !empty($featured_posts_widget_height) ? esc_attr($featured_posts_widget_height) : ''; ?>" />
        </p>

        <p>

            <label for="<?php echo $this->get_field_id('featured_posts_widget_autoplay'); ?>"><?php _e('Autoplay: <br> Slide automatically or wait for a click?', 'x2pp'); ?></label>
            <select class="widefat" id="<?php echo $this->get_field_id('featured_posts_widget_autoplay'); ?>" name="<?php echo $this->get_field_name('featured_posts_widget_autoplay'); ?>" >
                <?php  if(esc_attr($featured_posts_widget_autoplay) == 'off') { ?>
                    <option value="off" selected="selected" >off</option>;
                    <option value="on">on</option>
                <?php }else{ ?>
                    <option value="off">off</option>;
                    <option value="on" selected="selected">on</option>
                <?php } ?>
            </select>
        </p>

        <p id="<?php echo $this->get_field_id('slideshow_time_wrap'); ?>">
            <label for="<?php echo $this->get_field_id('featured_posts_widget_slideshow_time'); ?>"> <?php _e("Sliding time: <br> Define the sliding time in ms (without 'ms', just the plain integer). Default is 5000ms.", 'x2pp'); ?> </label>
            <input class="widefat" id="<?php echo $this->get_field_id('featured_posts_widget_slideshow_time'); ?>" name="<?php echo $this->get_field_name('featured_posts_widget_slideshow_time'); ?>" type="text" value="<?php echo !empty($featured_posts_widget_slideshow_time) ? esc_attr($featured_posts_widget_slideshow_time) : ''; ?>" />
        </p>
        <script>
            jQuery(document).ready(function() {
              jQuery('#<?php echo $this->get_field_id('featured_posts_widget_autoplay'); ?>').change(function(){
                if(jQuery(this).val() == 'off' ){
                    jQuery('#<?php echo $this->get_field_id('slideshow_time_wrap'); ?>').hide();
                }else{
                    jQuery('#<?php echo $this->get_field_id('slideshow_time_wrap'); ?>').show();
                }
              }).trigger('change');
            });
        </script>
        <?php
    }
}
function X2_Carousel_register_widgets() {
	register_widget( 'X2_Carousel_Posts_Widget' );
}

add_action( 'widgets_init', 'X2_Carousel_register_widgets' );?>