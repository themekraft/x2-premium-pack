<?php
/**
 * Contains pro version code and enabe it
 */
if(!defined('is_pro')){
    define('is_pro', true);
}

/**
 * Adding some admin styles
 *
 * @author Konrad Sroka
 * @package x2_pro
 * @since 1.7.0
 */
function x2pro_admin_styles_and_scripts($hook) {

    // load the admin.css
    wp_register_style( 'x2pro-admin-css', plugins_url( 'x2-premium-pack/includes/admin/css/admin.css' ) );
    wp_enqueue_style( 'x2pro-admin-css' );

    // load the admin.js
    wp_enqueue_script('x2pro-admin-js', plugins_url('/admin/js/admin.js', __FILE__));

}
add_action( 'admin_enqueue_scripts', 'x2pro_admin_styles_and_scripts' );

/**
 * Adding premium support button
 *
 * @author Konrad Sroka
 * @package x2_pro
 * @since 1.7
 */
function x2_add_support_button() { ?>
    <a class="button button-primary" onClick="script: Zenbox.show(); return false;" class="button secondary"  href="#" title="<?php _e('Submit an email support ticket and get personal help now!', 'x2pp'); ?>" style="margin-right: 3px;">
        <?php _e('Submit a Support Ticket', 'x2pp'); ?>
    </a>
    <?php
}
add_action( 'x2_support_add', 'x2_add_support_button' );

/**
 * Child Theme Wizard
 *
 * @author Sven Lehnert
 * @package x2_pro
 * @since 1.7.0
 */
function x2_theme_settings_screen_child_theme(){ ?>

    <br /><hr /><br />
    <h2><?php _e("Copy Child Theme", "x2pp"); ?></h2>
    <p><?php _e('This will copy the child theme from the premium pack plugin to your theme folder.', 'x2pp'); ?></p><br>
    <a href="#" id="copy_child_theme" class="button-primary button"><?php _e('Copy Child Theme Now', 'x2pp'); ?></a>
    <br />
    <br />
    <h3><?php _e('How to start:', 'x2pp'); ?></h3>
    <p><b><?php _e('Step 1', 'x2pp'); ?></b><br /><?php _e('After creating your child theme you need to go to your theme folder and rename the theme.', 'x2pp'); ?></p>
    <p><b><?php _e('Step 2', 'x2pp'); ?></b><br /><?php _e('Make sure you rename the folder and the theme name in the style.css too.', 'x2pp'); ?></p>
    <p><b><?php _e('Step 3', 'x2pp'); ?></b><br /><?php _e('And then just activate your new child theme!', 'x2pp'); ?></p>
    <p><i><small><?php _e('Note: every time you click on create child theme the theme "chane-my-name" will be overwritten.', 'x2pp'); ?></small></i></p>
    <br />

    <?php
}
add_action('x2_theme_settings_screen', 'x2_theme_settings_screen_child_theme');

/**
 * Copy the Child Theme
 *
 * @author Sven Lehnert
 * @package x2_pro
 * @since 1.7.0
 */
function x2_xcopy_ajax(){
    x2_xcopy(X2_CHILD_THEME, get_theme_root());
    die();
}
add_action('wp_ajax_x2_xcopy_ajax', 'x2_xcopy_ajax');
add_action('wp_ajax_nopriv_x2_xcopy_ajax', 'x2_xcopy_ajax');

function x2_xcopy($src, $dest) {
    foreach (scandir($src) as $file) {

        $srcfile = rtrim($src, '/') .'/'. $file;
        $destfile = rtrim($dest, '/') .'/'. $file;

        if (!is_readable($srcfile)) { continue; }

        if ($file != '.' && $file != '..'){
            if (is_dir($srcfile)) {
                if (!file_exists($destfile)) {
                    mkdir($destfile);
                }
                foreach (scandir($srcfile) as $file2) {

                    $srcfile2 = $srcfile .'/'. $file2;
                    $destfile2 = $destfile .'/'. $file2;
                    copy($srcfile2, $destfile2);
                }
            } else {
                 copy($srcfile, $destfile);
            }
        }
    }
}

/**
 * Flush selects and checkboxes in page "Page Create"
 *
 * @author Sven Lehnert
 * @package x2_pro
 * @since 1.7.0
 */
function x2_flush_metabox_to_defaults(){
    if(!empty($_GET['post_type']) && $_GET['post_type'] == 'page' && empty($_GET['action'])){
       wp_enqueue_script('flush_to_defaults', plugin_dir_path( __FILE__ ). '/includes/js/flush_to_defaults.js', array('jquery'));
   }
}
add_action('admin_enqueue_scripts', 'x2_flush_metabox_to_defaults');

/**
 * Add additional color schemes Color scheme options
 *
 * @author Sven Lehnert
 * @package x2_pro
 * @since 1.7.0
 *
 * @param array $schemes
 * @return array schemes with additional options
 */
function x2_pro_add_color_scheme($schemes){
    $additinal_schemes = array( 'black', 'natural');
    return array_merge($schemes, $additinal_schemes);
}
add_filter('x2_get_color_scheme', 'x2_pro_add_color_scheme');

/**
 * Add tabs in theme settings accordion
 *
 * @author Sven Lehnert
 * @package x2_pro
 * @since 1.7.0
 * @param array $options
 * @param string $tab_id
 * @return array new options
 */
function x2_pro_cap_get_options( $options, $tab_id ) {
    if($tab_id == 'general'){

        $pro_options = array(
            new TextOption(
                __("Website width", "x2pp"),
                __("Just type in the number, either in px or %. Default is 1000.", "x2pp") . '<br>' .
                    __("Tip: If you use the full-width slider, don't make your site bigger than 1000px, or use the normal slider with preview.", "x2pp")  . '<br>' .
                    __("Actually we recommend to leave it around 960-1000px.", "x2pp"),
                "website_width",
                "1000",
                "",
                "start",
                __("Website width", "x2pp")),
            new DropdownOption(
                __("Fluid or static width?", "x2pp"),
                __("Do you want your layout fluid(%) or static(px)?", "x2pp") . '<br>' .
                    __("Notes: when you use the slideshow, better don't use fluid width.", "x2pp") . '<br>' .
                    __("And if you use the slideshow shadow, it only looks nice with a static width between 990 and about 1100px.", "x2pp"),
                "website_width_unit",
                array('px', '%'),
                "",
                'end'),
            new DropdownOption(
                __("Corner radius", "x2pp"),
                __("Do you want your corners to be rounded?", "x2pp"),
                "container_corner_radius",
                array('rounded', 'not rounded'),
                "not rounded"),
            new DropdownOption(
                __("Show or hide badge?", "x2pp"),
                __("Show or hide the announcement badge in the left top corner?", "x2pp"),
                "body_badge_show",
                array('show', 'hide', 'just my image'),
                "show",
                'start',
                __('Corner badge', "x2pp")),
            new TextOption(
                __("Badge link url", "x2pp"),
                __("If you add an url here the badge will be linked automatically!", "x2pp"),
                "body_badge_link",
                "#",
                "",
                false),
            new TextOption(
                __("Badge text", "x2pp"),
                __("Write your own text on the badge!", "x2pp"),
                "body_badge_text",
                __("Your text, link, image.", "x2pp"),
                "",
                "",
                false),
            new ColorOption(
                __("Badge text color", "x2pp"),
                __("Change your badge text color. Default is #AAAAAA.", "x2pp"),
                "badge_text_color",
                "AAAAAA",
                '',
                ''),
            new ColorOption(
                __("Badge text color hover", "x2pp"),
                __("Change your badge text color for the mouse over effect. Default is #999.", "x2pp"),
                "badge_text_color_hover",
                "999999",
                '',
                ''),
            new FileOption(
                __("Badge background image", "x2pp"),
                __("Add your own image for the badge background. Upload or insert url.", "x2pp"),
                "body_badge_img",
                "",
                'end'),
            new DropdownOption(
                __("Avatar style", "x2pp"),
                __("Do you want your avatars squares or circles?", "x2pp"),
                "avatar_circles",
                array('circles', 'squares'),
                "circles"),
            new DropdownOption(
                __("Admin Bar", "x2pp"),
                __("Show or hide wp admin bar on top?", "x2pp"),
                "admin_bar_position",
                array('show', 'hide'),
                "hide"),
            new FileOption(
                __("Favicon", "x2pp"),
                __("Insert your own favicon image. Upload or insert url.", "x2pp"),
                "favicon"),
            // Login
            new FileOption(
                __("Login page logo", "x2pp"),
                __("Insert your own image for the login page. Upload or insert url.", "x2pp"),
                "bg_loginpage_img",
                "",
                "start",
                __("Login", "x2pp")),
            new TextOption(
                __("Login logo height", "x2pp"),
                __("Define the login logo height, the width should be 326px max", "x2pp"),
                "login_logo_height",
                "",
                "",
                false),
            new FileOption(
                __("Login page background image", "x2pp"),
                __("Insert your own image for the login page background. Upload or insert url.", "x2pp"),
                "bg_loginpage_body_img",
                "","",false),
            new ColorOption(
                __("Login page background colour", "x2pp"),
                __("Change login page background colour", "x2pp"),
                "bg_loginpage_body_color",
                "",
                'end'),
            new DropdownOption(
                __("404 style", "x2pp"),
                __("Choose a style for your 404 page!", "x2pp") . '<br>' .
                    __("The style 'broken' is default and uses a lot of nice CSS3 :)", "x2pp") . '<br>' .
                    __("What's 404? If a page doesn't exists you will be lead to your 404 error page.", "x2pp"),
                "errorpage_style",
                array('chaos', 'normal'),
                "chaos",
                'start',
                '404'),
            new TextOption(
                __("Chaos Message", "x2pp"),
                __("Just for the 'Chaos' Style (see the option above).", "x2pp") . '<br>' .
                    __("Write your own chaos message instead of 'Something's wrong here'.", "x2pp"),
                "errorpage_chaos_msg",
                "",
                "",
                false),
            new TextOption(
                __("404 Pagetitle", "x2pp"),
                __("Write your own title instead of the boring 'Page not found'. :)", "x2pp"),
                "errorpage_title",
                "",
                "",
                false),
            new TextOption(
                __("404 Search Message", "x2pp"),
                __("Write your own short text above the search.", "x2pp") . '<br>' .
                    __("Note: Only working when you show the search (see option below).", "x2pp"),
                "errorpage_search_msg",
                "",
                "",
                false),
            new DropdownOption(
                __("Hide search", "x2pp"),
                __("You can hide the search for users including the message above the search.", "x2pp"),
                "errorpage_search_show",
                array('show', 'hide'),
                "show",
                'end'),
            new TextOption(
                __("Add scripts to head", "x2pp"),
                __("...for google fonts, analytics, etc.", "x2pp") . '<br>' .
                    __("Here you can add stuff right before the end of the head tag.", "x2pp"),
                "add_to_head",
                "",
                "",
                "start",
                __("Scripts", "x2pp")),
            new TextOption(
                __("Add scripts to footer", "x2pp"),
                __("...for analytics, ads, etc.", "x2pp") . '<br>' .
                    __("Here you can add stuff right before the end of the footer tag.", "x2pp"),
                "add_to_footer",
                "",
                "",
                "end"),
        );
        $options = array_merge($options, $pro_options);

    } else if($tab_id == 'slideshow'){

        $pro_options = array(
            new TextOption(
                __("Amount", "x2pp"),
                __("Define the amount of posts. Note: This option works NOT for the slider with preview thumbnails.", "x2pp") . '<br>' .
                    __("See  in the option section 'Slideshow Style'.", "x2pp"),
                "slideshow_amount",
                ""),
            new TextOption(
                __("Post type", "x2pp"),
                __("Define the post type to display instead of posts.", "x2pp") . '<br>' .
                    __("For pages write 'page', for a custom post type the name of the cutsom post type, e.g. 'radio'", "x2pp"),
                "slideshow_post_type",
                ""),
            new TextOption(
                __("Page IDs", "x2pp"),
                __("Page IDs, comma separated. Just working if you use post types instead of categories.", "x2pp"),
                "slideshow_show_page",
                ""),
            new TextOption(
                __("Sliding time", "x2pp"),
                __("Define the sliding time in ms (without 'ms', just the plain integer). Default is 5000ms.", "x2pp") . '<br>' .
                    __("This option works for flux and nivo sliders. ", "x2pp"),
                "slideshow_time", ""),
            new TextOption(
                __("Order posts by", "x2pp"),
                "* orderby=author<br>
                * orderby=date<br>
                * orderby=title<br>
                * orderby=modified<br>
                * orderby=menu_order - ". __("used most often for Pages (Order field in the Edit Page -> Attributes box) and attachments (the integer fields in the Insert / Upload Media -> Gallery dialog), but could be used for any post type with distinct menu_order values (they all default to 0).", 'x2pp') ."<br>
                * orderby=parent<br>
                * orderby=ID<br>
                * orderby=rand<br>
                * orderby=meta_value - ". __("Note: A meta_key=keyname must also be present in the query. Note also that the sorting will be alphabetical which is fine for strings (i.e. words), but can be unexpected for numbers (e.g. 1, 3, 34, 4, 56, 6, etc, rather than 1, 3, 4, 6, 34, 56 as you might naturally expect).", 'x2pp') ."<br>
                * orderby=meta_value_num - ". __("Order by numeric meta value (available with Version 2.8)", 'x2pp') ."<br>
                * orderby=none - ". __("No order (available with Version 2.8)", 'x2pp') ."<br>
                * orderby=comment_count - ". __("(available with Version 2.9)", 'x2pp') ."<br>",
                "slideshow_orderby",
                ""),
        );
        $pro_slide_stules = array(
            new DropdownOption(
                __("Autoplay", "x2pp"),
                __("Slide automatically or wait for a click?", "x2pp"),
                "slideshow_autoplay",
                array('on', 'off'),
                'on',
                ''),
            new DropdownOption(
                __("Pagination", "x2pp"),
                __("Show the pagination?", "x2pp"),
                "slideshow_pagination",
                array('on', 'off'),
                'on',
                ''),
            new DropdownOption(
                __("Caption", "x2pp"),
                __("Show just the images - or also titles and excerpts (excerpts in default and full width slideshow styles only)?", "x2pp"),
                "slideshow_caption",
                array('on', 'off'),
                'on',
                ''),
            new DropdownOption(
                __("Slides as links?","x2pp"),
                '',
                "slideshow_links",
                array('yes', 'no'),
                'yes',
                ''),
            new DropdownOption(
                __("Controls", "x2pp"),
                __("Show controls to navigate?", "x2pp"),
                "slideshow_controls",
                array('on', 'off'),
                'on',
                ''),
            new DropdownOption(
                __("Shadow", "x2pp"),
                __("Select if you'd like to have a shadow under the top slideshow.", "x2pp") .'<br>'.
                    __("Note: just for bright background and static width between 990 and about 1100 pixels.", "x2pp"),
                "slideshow_shadow",
                array('sharper shadow', 'shadow', 'no shadow'),
                'no shadow',''),
       );

        $last_slide_style_item = array_pop($options);
        $options = array_merge($options, $pro_slide_stules);
        array_push($options, $last_slide_style_item);
        $options = array_merge($options, $pro_options);

    } else if($tab_id == 'footer'){

        $pro_options = array(
            new BooleanOption(
                __("Show credits links in footer", "x2pp"),
                __("Show the credits liks in the footer.", "x2pp"),
                "disable_credits_footer",
                true,
                'start',
                __("Credit links", "x2pp")),
            new TextOption(
                __("Your own credits links in footer", "x2pp"),
                __("Write in your own credits - links - text. You can use HTML.", "x2pp"),
                "my_credits_footer",
                "",
                '',
                'end'),
        );
        $options = array_merge($options, $pro_options);

    } else if ( $tab_id == 'menu-bottom' ) {

        $pro_options = array(
            new ColorOption(
                __( "Menu font colour &raquo; mouse over", "x2" ),
                __( "Change font colour of mouse over state.", "x2" ),
                "menu_link_color_hover"
            ),
            new ColorOption(
                __( "Menu font colour &raquo; drop down list &raquo; mouse over", "x2" ),
                __( "Change font colour of HOVERED DROP DOWN menu item <br>
                (when the mouse moves over it).", "x2" ),
                "menu_link_color_dd_hover"
            )
        );


        $options = array_merge( $options, $pro_options );

    }

    return $options;
}
add_filter('x2_cap_get_options', 'x2_pro_cap_get_options', 100, 2);

/**
 * Filters values array for `posts_lists_title` option.
 */
add_filter( 'x2_posts_lists_title_options', 'x2_add_posts_lists_title_options' );
function x2_add_posts_lists_title_options( $options ) {
    array_push( $options, 'show' );
    array_push( $options, 'hide' );

    return $options;
}

function x2_add_category_fields($options, $id, $prefix, $post_types){
    $options_categories = array();
    $options_categories_obj = get_categories();
    $options_categories['all-categories'] = 'All categories';
    foreach ($options_categories_obj as $category) {
        $options_categories[$category->term_id] = $category->cat_name.' ('.$category->count.')';
    }
    array_push($options,
            array(
                'name'    => __('Featured post categories', 'x2pp'),
                'desc'    => __('You can select the category the posts should be taken from.', 'x2pp'),
                'id'      => $prefix . 'featured_posts_category',
                'type'    => 'multicheck',
                'options' => $options_categories
                ,
            ));
     return $options;
}
add_filter('x2_add_meta_fields', 'x2_add_category_fields', 15, 4);

function x2_add_sticky_fields($options, $id, $prefix, $post_types){
    array_push($options,
        array(
            'name' => __('Show only sticky posts', 'x2pp'),
            'id'   => $prefix . 'featured_posts_show_sticky',
            'type' => 'checkbox',
        ));
     return $options;
}
add_filter('x2_add_meta_fields', 'x2_add_sticky_fields', 20, 4);
function x2_add_post_type_fields($options, $id, $prefix, $post_types){
    $options_post_types = array();
    foreach ($post_types  as $post_type ) {
           $options_post_types[$post_type] = $post_type;
    }
    array_push($options,
        array(
            'name'    => __('Post type', 'x2pp'),
            'desc'    => __("Define the post type to display instead of posts. For pages select <code>page</code>, for a custom post type the name of the cutsom post type, e.g. 'product'", "x2pp"),
            'id'      => $prefix . 'featured_posts_post_type',
            'type'    => 'multicheck',
            'options' => $options_post_types,
        ));

    return $options;
}
add_filter('x2_add_meta_fields', 'x2_add_post_type_fields', 25, 4);

function x2_add_post_ids_fields($options, $id, $prefix, $post_types){
    array_push($options,
        array(
            'name' => __('Page IDs', 'x2pp'),
            'desc' => __("Page IDs, comma separated. Just working if you use post types instead of categories", "x2pp"),
            'id'   => $prefix . 'featured_posts_show_pages_by_id',
            'type' => 'text_medium',
        ));
     return $options;
}
add_filter('x2_add_meta_fields', 'x2_add_post_ids_fields', 30, 4);

function x2_add_jquery_effect_fields($options, $id, $prefix, $post_types){
    array_push($options,
        array(
            'name'    => __('Choose jQuery effect', 'x2pp'),
            'desc'    => __('Select pagination jQuery effect.', 'x2pp'),
            'id'      => $prefix . 'featured_posts_pagination_ajax_effect',
            'type'    => 'select',
            'options' => array(
                array( 'name' => __('fadeOut - fadeIn', 'x2pp'), 'value' => 'fadeOut_fadeIn', ),
                array( 'name' => __('slideUp - slidedown', 'x2pp'), 'value' => 'slideUp_slidedown', ),
                array( 'name' => __('animate hide - show', 'x2pp'), 'value' => 'hide_show', ),
                array( 'name' => __('slide - left - slide - right', 'x2pp'), 'value' => 'slide_left_slide_right', ),
            ),
        ));
     return $options;
}
//add_filter('x2_add_meta_fields', 'x2_add_jquery_effect_fields', 50, 4);

function x2_add_pro_meta_values($optons, $item_name, $prefix, $post_types){

    if($item_name == 'x2_slideshow'){
        // Pull all the pages into an array
        $options_post_types = array();
        if(!empty($post_types)){
            foreach ($post_types  as $post_type ) {
                $options_post_types[$post_type] = $post_type;
            }
        }
        $pro_fields = array(
                array(
                    'name'    => __('Yes / No links in slideshow','x2pp'),
                    'desc'    => __('Slides as links?','x2pp'),
                    'id'      => $prefix . 'slideshow_links',
                    'type'    => 'select',
                    'options' => array(
                        array( 'name' => 'yes', 'value' => 'yes', ),
                        array( 'name' => 'no', 'value' => 'no', ),
                    ),
                ),
                array(
                    'name' => __('Show only sticky posts', 'x2pp'),
                    'id'   => $prefix . 'slideshow_sticky',
                    'type' => 'checkbox',
                ),
                array(
                    'name' => __('Amount', 'x2pp'),
                    'desc' => __('Define the amount of posts. This option works only with flux slider and nivo slider.', 'x2pp'),
                    'id'   => $prefix . 'slideshow_amount',
                    'type' => 'text_small',
                ),
                array(
                    'name'    => __('Post type', 'x2pp'),
                    'desc'    => __("Define the post type to display. For pages select <code>page</code>, for a custom post type the name of the cutsom post type, e.g. 'product'", "x2pp"),
                    'id'      => $prefix . 'slideshow_post_type',
                    'type'    => 'multicheck',
                    'options' => $options_post_types,
                ),
                array(
                    'name'    => __('Page IDs','x2pp'),
                    'desc'    => __("Page IDs, comma separated. Just working if you use post types instead of categories", "x2pp"),
                    'id'      => $prefix . 'slideshow_show_page',
                    'type'    => 'text_medium',
                ),
                array(
                    'name'    => __('Order posts by', 'x2pp'),
                    'id'      => $prefix . 'slideshow_orderby',
                    'type'    => 'text_medium',
                ),
                array(
                    'name'    => __('Slideshow effect', 'x2pp'),
                    'desc'    => __('Select the slideshow effect. Default is random.', 'x2pp'),
                    'id'      => $prefix . 'slideshow_effect',
                    'type'    => 'select',
                    'options' => array(
                        array( 'name' => __('random', 'x2pp'), 'value' => 'random', ),
                        array( 'name' => __('bars', 'x2pp'), 'value' => 'bars', ),
                        array( 'name' => __('blinds', 'x2pp'), 'value' => 'blinds', ),
                        array( 'name' => __('blocks', 'x2pp'), 'value' => 'blocks', ),
                        array( 'name' => __('blocks2', 'x2pp'), 'value' => 'blocks2', ),
                        array( 'name' => __('concentric', 'x2pp'), 'value' => 'concentric', ),
                        array( 'name' => __('dissolve', 'x2pp'), 'value' => 'dissolve', ),
                        array( 'name' => __('slide', 'x2pp'), 'value' => 'slide', ),
                        array( 'name' => __('warp', 'x2pp'), 'value' => 'warp', ),
                        array( 'name' => __('zip', 'x2pp'), 'value' => 'zip', ),
                        array( 'name' => __('bars3d', 'x2pp'), 'value' => 'bars3d', ),
                        array( 'name' => __('cube', 'x2pp'), 'value' => 'cube', ),
                        array( 'name' => __('tiles3d', 'x2pp'), 'value' => 'tiles3d', ),
                        array( 'name' => __('turn', 'x2pp'), 'value' => 'turn', ),
                    ),
                ),
                array(
                    'name'    => __('Slideshow autoplay', 'x2pp'),
                    'desc'    => __('Autoplay or wait for a click of the user?', 'x2pp'),
                    'id'      => $prefix . 'slideshow_autoplay',
                    'type'    => 'select',
                    'options' => array(
                        array( 'name' => __('on', 'x2pp'), 'value' => 'on', ),
                        array( 'name' => __('off', 'x2pp'), 'value' => 'off', ),
                    ),
                ),
                array(
                    'name'    => __('Slideshow pagination', 'x2pp'),
                    'desc'    => __('Show the pagination as dots under the slideshow?', 'x2pp'),
                    'id'      => $prefix . 'slideshow_pagination',
                    'type'    => 'select',
                    'options' => array(
                        array( 'name' => __('on', 'x2pp'), 'value' => 'on', ),
                        array( 'name' => __('off', 'x2pp'), 'value' => 'off', ),
                    ),
                ),
                array(
                    'name'    => __('Slideshow controls', 'x2pp'),
                    'desc'    => __('Show slideshow controls, as arrows left and right', 'x2pp'),
                    'id'      => $prefix . 'slideshow_controls',
                    'type'    => 'select',
                    'options' => array(
                        array( 'name' => __('on', 'x2pp'), 'value' => 'on', ),
                        array( 'name' => __('off', 'x2pp'), 'value' => 'off', ),
                    ),
                ),
                array(
                    'name'    => __('Slideshow caption', 'x2pp'),
                    'desc'    => __('Show just the images or also titles and excerpts (excerpts in default and full width slideshow styles only)?', 'x2pp'),
                    'id'      => $prefix . 'slideshow_caption',
                    'type'    => 'select',
                    'options' => array(
                        array( 'name' => __('on', 'x2pp'), 'value' => 'on', ),
                        array( 'name' => __('off', 'x2pp'), 'value' => 'off', ),
                    ),
                ),
                array(
                    'name'    => __('Slideshow Shadow', 'x2pp'),
                    'desc'    => __('Select if you like to have a shadow under the top slideshow. Note: just for bright background and static width between 990 and about 1100 pixels.', 'x2pp'),
                    'id'      => $prefix . 'slideshow_shadow',
                    'type'    => 'select',
                    'options' => array(
                        array( 'name' => __('no shadow', 'x2pp'), 'value' => 'no shadow', ),
                        array( 'name' => __('shadow', 'x2pp'), 'value' => 'shadow', ),
                        array( 'name' => __('sharper shadow','x2pp'), 'value' => 'sharper shadow', ),
                    ),
            )
        );
        $optons = array_merge($optons, $pro_fields);
    }
    return $optons;
}
add_filter('x2_add_slideshow_meta_fields', 'x2_add_pro_meta_values', 10 , 4);

function x2_pro_load_constants(){
    global $cap, $post;

    $sidebar_position = $cap->sidebar_position;
    $tpl = !empty($post) ? get_post_meta( $post->ID, '_wp_page_template', true ) : 'default';

    if( $tpl != 'default' && !is_search() ){
        switch ($tpl) {
            case '_pro/left-sidebar.php': $cap->rightsidebar_width = 0; break;
            case '_pro/right-sidebar.php': $cap->leftsidebar_width = 0; break;
            case 'full-width.php': $cap->leftsidebar_width = 0; $cap->rightsidebar_width = 0; break;
        }
    } else {
        switch ($sidebar_position) {
            case __('left','x2pp') : $cap->rightsidebar_width = 0; break;
            case __('right','x2pp') : $cap->leftsidebar_width = 0; break;
            case __('none','x2pp') : $cap->leftsidebar_width = 0; $cap->rightsidebar_width = 0; break;
            case __('full-width','x2pp') : $cap->leftsidebar_width = 0; $cap->rightsidebar_width = 0; break;
        }
    }

}
add_action( 'bp_head', 'x2_pro_load_constants', 2 );

// Filters the array of values for option 'menu_x'.
add_filter( 'x2_option_menu_x', 'x2_pro_option_menu_x' );
function x2_pro_option_menu_x( $args ) {
    $args[] = 'center';

    return $args;
}

// Adds a `Sidebar Position` metabox.
add_filter( 'cmb_meta_boxes', 'x2_pro_metaboxes', 9 );
function x2_pro_metaboxes( $meta_boxes ) {
    $prefix = '_x2_pro_page_';

    $meta_boxes[] = array(
        'id'         => 'sidebar_position',
        'title'      => __( 'Sidebar Position', 'x2pp' ),
        'pages'      => array( 'page', 'post' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true,
        'fields'     => array(
            array(
                  'name'    => __( 'Define your default layout', 'x2pp' ),
                  'id'      => $prefix . 'sidebar_position',
                  'type'    => 'select',
                  'options' => apply_filters( 'x2_pro_sidebar_position_metabox_options', array(
                      array( 'name' => __('right', 'x2pp'), 'value' => 'right' ),
                      array( 'name' => __('left and right', 'x2pp'), 'value' => 'left and right' ),
                      array( 'name' => __('left', 'x2pp'), 'value' => 'left' ),
                      array( 'name' => __('full-width', 'x2pp'), 'value' => 'full-width' ),
                  ) ),
            ),
        )
    );

    return $meta_boxes;
}