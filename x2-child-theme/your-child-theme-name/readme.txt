=== Your Theme Name ===
Contributors: your names, you can also just use the "Author:" tag for a single author
Tags: add your tags here
Requires at least: 3.5
Tested up to: 3.7.1
Stable tag: 0.0.1
License: GNU General Public License, Version 3.0 
License URI: http://www.gnu.org/licenses/gpl-3.0.html



== Description == 

This is a custom x2 child theme! 



== Installation == 

1. Go to your admin dashboard to Appearance -> Themes 

2. Click on "upload" 

3. Upload the zipped theme file from your computer 

4. When uploaded and installed, click on "activate".. 

And you're finished :)  



== Thanks and Credits == 



== Changelog ==



== Upgrade Notice == 

