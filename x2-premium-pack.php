<?php
/*
 Plugin Name: X2 Premium Pack
 Plugin URI:  http://themekraft.com/store/x2-premium-pack/
 Description: The X2 Premium Pack provides additional theme options, single page options and a prepared child theme. And best of all: Get instant personal support, just 1 click from your theme options page.
 Version: 1.7.8
 Author: Sven Lehnert
 Author URI: http://themekraft.com/members/svenl77/
 Licence: GPLv3

 *****************************************************************************
 *
 * This script is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 ****************************************************************************

 * This is the x2 Premium Extension
 *
 * It was a premium theme before, now we made a free theme and a premium extension out of it!
 *
 * Simply activate and you will get more theme options and single page options added!
 *
 * You can also find a ready-to-use child theme inside this folder.
 *
 * There you got more page templates added already and you can start customizing right away!
 *
 * Detailed documentation on how to start with x2 Premium Pack will follow after this weekend!
 *
 * If you have any questions in the meantime, just drop an email to support@themekraft.com
 *
 * Cheers!
 *
 *
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

define('x2_premium_pack', '1.7.8' );

define('X2_CHILD_THEME', plugin_dir_path( __FILE__ ).'x2-child-theme/' );

require_once( plugin_dir_path( __FILE__ ) . 'includes/helper-functions.php');

require_once( plugin_dir_path( __FILE__ ) . 'includes/shortcodes.php');

require_once( plugin_dir_path( __FILE__ ) . 'includes/widgets/carousel-posts-widget.php');

/**
 * Load the WooCommerce API Manager functions and classes
 */

function x2_plugin_file() {
	return __FILE__;
}

function x2_plugin_url() {
	return plugins_url( '/', __FILE__ );
}

function x2_plugin_path() {
	return plugin_dir_path( __FILE__ );
}

function x2_plugin_name() {
	return untrailingslashit( plugin_basename( __FILE__ ) );
}

require_once( plugin_dir_path( __FILE__ ) . 'includes/resources/api-manager/api-manager.php');


/**
 * Load plugin textdomain.
 *
 * @since 1.0.0
 */
function x2pp_load_textdomain() {
  load_plugin_textdomain( 'x2pp', false, dirname( plugin_basename( __FILE__ ) ) . '/langs/' );
}
add_action( 'plugins_loaded', 'x2pp_load_textdomain' );